<!doctype html>

   <?php include'include/db.php'; ?>

<html lang="en">
<?php require'include/head.php'; ?>


<body>

<!--Loader-->


<!--Top bar-->

<header id="main-navigation">
  <?php
    require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">

<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Mjekët</h2>
         <div class="page_link">
         <a href="index.php">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="mjeket.php"><font color="red">Mjekët</font></a></span>
         </div>
      </div>
    </div>
  </div>
</div>  

</section>





<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">Të Gjithë</a></li>
             <li><a href="javascript:;" data-filter=".hod" class="actives filter">Doktorët</a></li>
             <li><a href="javascript:;" data-filter=".heart" class="filter">Stafi i mesem </a></li>
<!--
             <li><a href="javascript:;" data-filter=".skin" class="filter">Infermierët</a></li>
             <li><a href="javascript:;" data-filter=".health" class="filter">Departamenti</a></li>
-->
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
          
          
           <?php 
                 $query ="SELECT s.emri, s.mbiemri, sp.emri pozita, s.profesioni, s.pershkrimi,s.foto
FROM staf s
INNER JOIN staf_pozita sp ON sp.id_staf_pozita = s.pozita where s.pozita=2";
              
              $select_staf = mysqli_query($dbc, $query);
              
                while($row = mysqli_fetch_assoc($select_staf)){
                   $emri = $row['emri'];
                   $mbiemri = $row['mbiemri'];
                   $pozita = $row['pozita'];
                   $profesioni = $row['profesioni'];
                   $pershkrimi = $row['pershkrimi'];
                   $foto = $row['foto'];
    
                    ?>
             
            
                
            <div class="col-1-3 mix work-item heart heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                  
                  
                    <img src="<?php echo $foto ; ?>" alt="work"/>
                    
                    <div class="overlay">
                      <div class="overlay-inner">
                        <ul class="social_icon">
                        <li><a href="#" class="facebook"><i class="icon-facebook5"></i></a></li>
                        <li><a href="#" class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#" class="google"><i class="icon-google"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content text-left">
                  
                    <h3><?php echo ' '.$emri.' '.$mbiemri ?></h3>
                    <small><?php echo $pozita ?></small>
                    <p> <?php echo $pershkrimi ?></p>
                  </div>
                </div>
              </div>
            </div> 
               
              <?php } ?>
              
               <?php 
                 $query ="SELECT s.emri, s.mbiemri, sp.emri pozita, s.profesioni, s.pershkrimi,s.foto
FROM staf s
INNER JOIN staf_pozita sp ON sp.id_staf_pozita = s.pozita where s.pozita =1";
              
              $select_staf = mysqli_query($dbc, $query);
              
                while($row = mysqli_fetch_assoc($select_staf)){
                   $emri = $row['emri'];
                   $mbiemri = $row['mbiemri'];
                   $pozita = $row['pozita'];
                   $profesioni = $row['profesioni'];
                   $pershkrimi = $row['pershkrimi'];
                   $foto = $row['foto'];
    
                    ?>
             
            
                
            <div class="col-1-3 mix work-item hod heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                  
                  
                    <img src="<?php echo $foto ; ?>" alt="work"/>
                    
                    <div class="overlay">
                      <div class="overlay-inner">
                        <ul class="social_icon">
                        <li><a href="#" class="facebook"><i class="icon-facebook5"></i></a></li>
                        <li><a href="#" class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#" class="google"><i class="icon-google"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content text-left">
                  
                    <h3><?php echo $pozita.' '.$emri.' '.$mbiemri ?></h3>
                    <small><?php echo $profesioni ?></small>
                    <p> <?php echo $pershkrimi ?></p>
                  </div>
                </div>
              </div>
            </div> 
               
              <?php } ?>

               <?php 
                 $query ="SELECT s.emri, s.mbiemri, sp.emri pozita, s.profesioni, s.pershkrimi,s.foto
FROM staf s
INNER JOIN staf_pozita sp ON sp.id_staf_pozita = s.pozita where s.pozita =3";
              
              $select_staf = mysqli_query($dbc, $query);
              
                while($row = mysqli_fetch_assoc($select_staf)){
                   $emri = $row['emri'];
                   $mbiemri = $row['mbiemri'];
                   $pozita = $row['pozita'];
                   $profesioni = $row['profesioni'];
                   $pershkrimi = $row['pershkrimi'];
                   $foto = $row['foto'];
    
                    ?>
             
            
                
            <div class="col-1-3 mix work-item skin heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                  
                  
                    <img src="<?php echo $foto ; ?>" alt="work"/>
                    
                    <div class="overlay">
                      <div class="overlay-inner">
                        <ul class="social_icon">
                        <li><a href="#" class="facebook"><i class="icon-facebook5"></i></a></li>
                        <li><a href="#" class="twitter"><i class="icon-twitter4"></i></a></li>
                        <li><a href="#" class="google"><i class="icon-google"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content text-left">
                  
                    <h3><?php echo $pozita.' '.$emri.' '.$mbiemri ?></h3>
                    <small><?php echo $profesioni ?></small>
                    <p> <?php echo $pershkrimi ?></p>
                  </div>
                </div>
              </div>
            </div> 
               
              <?php } ?>
              
              
              
              
             
              
              
          </div>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>




<!--Footer-->
<footer class="padding-top dark">
 <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/functions.js" type="text/javascript"></script>

</body>
</html>
