<?php 
if(!($_SESSION['logged_in']))  {

     header("location:include/main-login2.php");

}
?>

<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="pull-left hidden-xs">Qendra Diagnostike Endokrinologjike - Elitaplus</p>
          <p class="pull-right"><i class="fa fa-phone" aria-hidden="true"></i>Tel. +377 44 204 240</p>
      </div>
    </div>
  </div>
</div>
    <div id="navigation" data-spy="affix" data-offset-top="20">
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="false"> 
            <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> 
            </button>
             <a class="navbar-brand"  href="index.php"><img src="images/logo_t.png" alt="logo" style="display: inline;" class="img-responsive"><span class="elita-logo"> &nbsp;&nbsp;Elita <span class="elita-logo-kuq">plus</span></span></a> 
         </div>
            <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right">
           
             <ul class="nav navbar-nav" id="nav-links">
                 <?php $page = "$_SERVER[REQUEST_URI]"; ?>
                <li <?php if ($page == '/elita12/index.php'){    echo "class='active'";}?> ><a id='ae' href="index.php"> Ballina</a></li>
                <li <?php if ($page == '/elita12/mjeket.php'){   echo "class='active'";}?> ><a id='ae' href="mjeket.php"> Mjeket</a></li>
                <li <?php if ($page == '/elita12/laboratori.php'){ echo "class='active'";}?> ><a id='ae' href="laboratori.php"> Laboratori</a></li>
                <li <?php if ($page == '/elita12/qmimet.php'){   echo "class='active'";}?> ><a id='ae' href="qmimet.php"> Çmimet</a></li>
                <li <?php if ($page == '/elita12/terminet.php'){ echo "class='active'";}?> ><a id='ae' href="terminet.php"> Terminet</a></li>
                <li <?php if ($page == '/elita12/foto.php'){     echo "class='active'";}?> ><a id='ae' href="foto.php"> Foto</a></li>
                <li <?php if ($page == '/elita12/kontakt.php'){  echo "class='active'";}?> ><a id='ae' href="kontakt.php"> Kontakt</a></li>
                <li <?php if ($page == '/elita12/lajmet.php'){   echo "class='active'";}?> ><a id='ae' href="lajmet.php"> Lajmet</a></li>


              </ul>
               
                
            </div>
         </nav>
         </div>
       </div>
     </div>
  </div>

