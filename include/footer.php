 <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Për në</h4>
        <hr class="half_space">
        <p class="half_space">Laboratori në kuadër të qendrës është i njohur anë e mbanë Kosovës dhe jashtë saj për Hormone, Tumor marker, biokimi. Risi janë analizat e sëmundjeve autoimune; a-CCP, ANA, cANCA, pANCA, ani Intrisik faktori, antiGliadin, antiTransglutaminse....shërbimet tjera: PEDIATRI, GASTROENTEROLOGU, NEFROLOGU, PULMOLOGU.</p>
        
      </div>
      <div class="col-md-3  col-sm-6 footer_column">
        <h4 class="heading">Lidhje të shpejta</h4>
        <hr class="half_space">
        <ul class="widget_links">
          <li><a href="index.php">Ballina</a></li>
          <li><a href="mjeket.php">Mjeket</a></li>
          <li><a href="laboratori.php">Laboratori</a></li>
          <li><a href="qmimet.php">Çmimet</a></li>
          <li><a href="terminet.php">Terminet</a></li>
          <li><a href="foto.php">Foto</a></li>
          <li><a href="kontakt.php">Kontakti</a></li>
           <li><a href="https://www.facebook.com/qendraelitaplus/?pnref=about.overview">Facebook</a></li>
          
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Lokacioni</h4>
        <hr class="half_space">
        
        <div id="result1" class="text-center"></div>   
             <div class="map">
            
                 <iframe src="https://www.google.com/maps/embed/v1/place?q=elita+plus+prishtine&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU" width="260" height="200" frameborder="0" style="border:0"></iframe> </div>

            
       
      
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Kontakti</h4>
        <hr class="half_space">
        <p>Qendra Diagnostike Endokrinologjike - Elitaplus, është qendër e specializuar për sëmundje Endokrinologjike.</p>
        <p class="address"><i class="icon-location"></i>Dardani, SU 6/7-1, Prishtinë.</p>
        <p class="address"><i class="fa fa-phone"></i>044/045/049 204-240.</p>
        <p class="address"><i class="icon-dollar"></i><a href="kontakt.php">info@elita-plus.com</a></p>
      </div> 
    </div>
    <div class="row">
     <div class="col-md-12">
        <div class="copyright clearfix">
          <p>Copyright &copy; 2016 ElitaPlus. Powerd by<a href="https://www.facebook.com/Crow-Development-611776368981759/" target="_blank"> Crow</a></p>
          <ul class="social_icon">
               <li><a href="https://www.facebook.com/qendraelitaplus/?pnref=about.overview" class="facebook"><i class="icon-facebook5"></i></a></li>
               <li><a href="https://twitter.com/elitaplus" class="twitter"><i class="icon-twitter4"></i></a></li>
               <li><a href="https://plus.google.com/106082473944214722052/reviews" class="google"><i class="icon-google"></i></a></li>
              </ul>
        </div>
      </div>
    </div>
  </div>