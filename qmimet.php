<!doctype html>

<?php include'include/db.php'; ?>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ElitaPlus</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/medical-guide-icons.css">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="css/zerogrid.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/loader.css">
<link rel="shortcut icon" href="images/logo1.jpg">

<!--<script src="js/calc.js" type="text/javascript"></script>-->
</head>

<body>

<!--Loader-->



<!--Top bar-->



<header id="main-navigation">
  <?php
  require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">

<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
       
        
         <h2 class="title">Çmimet e analizave</h2>



          <div class="page_link"><a href="index.php">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="qmimet.php"><font color="red">Çmimet e analizave</font></a></span></div>

      </div>

    </div>
  </div>
</div> 
    
</section>

<!--Pricings-->
<section id="pricing" class="padding">
 
  <div class="container" style="max-height:1000px;">
  
 
    <div class="row">
    
      
      <div class="col-md-12">
     <h2 class="heading">Cmimet</h2>
      <hr class="heading_space">
      <div class="col-md-6 col-sm-6">
        <div class="price">
        
        
<!--
          <div class="price_header clearfix">
          <span class="pull-left">Analizat</span>
          <span class="pull-right">Çmimet</span>
          </div>
-->
         <div class="row">
      <div class="col-md-12">
        

      <div class="service_wrapper">
      <ul class="items">      
                  <?php 

          $query = 'select id_kategoria_analizave,emri from kategori_analiza order by emri asc';
          
          $select_k_analiza = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['emri'];
           
          
          ?>
        <li>
          <a href="#"><?php echo $kategoria ?></a>
          <ul class="sub-items bg_grey">
                      <form  method="get" id="qek-box-form" onchange="kalkulimi()" action="#"  >
                      <?php $query= 'SELECT ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza,a.cmimi1 cmimi, a.pershkrimi
FROM kategori_analiza ka
INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave where ka.id_kategoria_analizave = '.$ka_id.' order by a.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['kategoria'];
            $analiza = $row['analiza'];
            $cmimi=$row['cmimi'];
            $pershkrimi = $row['pershkrimi'];
           
           
          
          ?>
          
            <li>
              <div class="row">
                <div class="col-md-12 accordion-text">
                  <div class="analiza-chekbox">       
                      <input id="qek-box" type="checkbox" name="analiza"><span class="analiza_n"><?php echo $analiza ?></span><span class="cmimi-1" style="float:right"><?php echo $cmimi ?></span><span style="float:right">&nbsp;&#8364;</span><br>
                </div> 
                  </div>
              </div>    
            </li>
                          <?php } ?>
                          </form>
          </ul>
        </li>
        
        
                <?php } ?>
      </ul>
    </div>
      </div>
    </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6" style="z-index:100" >
       
       
        <div class="price" >
        
        <div class="kalkulatori"   id="kalcprice">
        
        <h2 id="kalkemri" >Kalkulatori</h2>
        <hr class="heading_space">
<!--
          <div class="price_header clearfix">
          <span class="pull-left">Kalkulator</span>
          <span class="pull-right">Çmimi</span>
          
          </div>
-->
            
      <div class="row">
      
      <div class="col-md-12">
     
    
      
       
      <div class="service_wrapper " style="background-color:#f0f5f7">
      <ul id="items">      
                  
          </ul>
               
                </div> 
                <div class="totali">
<!--                 formaction="../index.php"-->
                   <i class="fa fa-print" aria-hidden="true"> <input type="button"   value="Print"  onclick="printDiv('kalcprice');"></i> 
                    
                 <h3 id="h3_total" style="float:right">Totali: € 0</h3>
                 </div>
                  </div>
            
            </div>
            
            
        </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Footer-->
<footer class="padding-top dark">
  <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/calc.js" type="text/javascript"></script>

<script src="js/on_load_ex_script.js" type="text/javascript"></script>
<script src="js/print.js" type="text/javascript"></script>
</body>
</html>
