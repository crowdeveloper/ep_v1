<?php require'include/db.php'; ?>
<!doctype html>
<html lang="en">
<?php require'include/head.php'; ?>
    
<body>
                      <style>
                      input::-webkit-calendar-picker-indicator{
    display: none;
}</style>

<script src="js/appointment.js" type="text/javascript"></script>
<script src="js/appointment_val.js" type="text/javascript"></script>


<!--Top bar-->   

<header id="main-navigation">
 <?php 
    require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">

<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Terminet</h2>
         <div class="page_link"><a href="index.php">Ballina</a><span><i class="fa fa-long-arrow-right"></i><a href="terminet.php"><font color="red">Terminet</font></a></span></div>
  </div>
</div>  
</div>  
</div>  

</section>





<section class="padding">
  <div class="container appointment_wrap padding-half">
    <div class="row">
      <div class="col-md-12">
       
          <div id='add_app'> <?php  if(isset($_SESSION['ins_app_succ'])){ echo $_SESSION['ins_app_succ']; $_SESSION['ins_app_succ']='';}?> </div>
          
          
        <h2>Cakto Terminet</h2>
        <p class="p_red"> Per konfirmim do te telefonoheni nga ne.</p>
       
        <div class="col-md-7 col-sm-8">
          
          <div class="row">
            <form class="callus" id="app_form"  method="post" action="include/insert_app.php?t" onsubmit="return app_val()" >
              <div class="row">
                
                 <div class="col-md-12">
                    <div id="result" class="text-center form-group"></div>
                 </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="text" id="app_name" name="app_name"  placeholder="Emri Mbiemri" />
                  </div>
                </div>
                  
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="email" name="app_email" id="app_email"  placeholder="Email"  />
                  </div>
                </div>
                  
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="tel"  name="app_telephone" id="app_telephone" placeholder="Nr.Telefonit"  />
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                   <?php 
                                  
                      $date = date('Y-m-d');                                                                                                        $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                      $check_sun = date("l",strtotime($date));
                      $check_sun = strtolower($check_sun);
                    if($check_sun == 'sunday'){
                      $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                        
                    }
                      
                      ?>
                      <!--    FORM     -->

                    <input type="date" id="app_date"  class="form-control " placeholder="Data e terminit"   onchange="on_ch(this.value);no_sun();w_date()" name="app_date"  value="<?php echo $date; ?>"/>
                          
                  </div>
                </div>
                  <div class="col-md-6">
                  <div class="form-group">
                      
                      <select class="form-control" placeholder="Koha e terminit HH:MM" id="app_time" name="app_time" >
                          
                          
                    </select>

                  </div>
                </div>
                  
                  <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Filan Fisteki" id="doc_name" name="doc_name" value="Dr.Erduan Sefedini" disabled  />
                  </div>
                </div>



                  <div class="form-group">
                     <div class="btn-submit button3">
                    <input type="submit" id="btn_app_submit" value="Cakto" />
                    </div>
                  </div>
                </div>
            </form>

              </div>
          </div>
        </div>
        <div class="col-md-5 col-sm-4"> </div>
      </div>
    </div>
</section>
 




<!--Footer-->
<footer class="padding-top bg_blue">
 <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>
 
    
 
    
<!--
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
        
-->
 
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script>
     
     $(function() {
    $("#app_date").datepicker({
        minDate:0,
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        beforeShowDay: function(date) {
            var day = date.getDay();
             return [(day != 0), ''];
    }
    });
});     
           
    </script>
   

<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/on_load_ex_script.js" type="text/javascript"></script>

</body>
</html>