<!doctype html>

   <?php include'include/db.php'; ?>

<html lang="en">
<?php require'include/head.php'; ?>

<body>
<script src="js/appointment.js" type="text/javascript"></script>
<script src="js/appointment_val.js" type="text/javascript"></script>

                   <style>
                      input::-webkit-calendar-picker-indicator{
    display: none;
}</style>

    
    
    
<!--Loader-->
<?php 
    require'include/loader.php';
    ?>

<!--Top bar-->   

<header id="main-navigation">
 <?php 
    require'include/header.php';
    ?>
</header>



<!-- REVOLUTION SLIDER -->			

<div id="rev_slider_35_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery35" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
				<!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
					<div id="rev_slider_35_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
						<ul>	<!-- SLIDE  -->
							<li data-index="rs-129" data-transition="fade" data-slotamount="default" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"  data-title="Kujdes në cilësi" data-description="Elitaplus, është qendër e specializuar
për sëmundje Endokrinologjike.">
								<!-- MAIN IMAGE -->
								<img src="images/home-page-pic2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
								<!-- LAYERS -->

								<!-- LAYER NR. 1 -->
                        <!--<div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" 
									 id="slide-129-layer-3" 
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
									 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
								    data-width="full"
									 data-height="full"
									 data-whitespace="normal"
									 data-transform_idle="o:1;"
						 
									 data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" 
									 data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
									data-start="1000" 
									data-basealign="slide" 
									data-responsive_offset="on" 
									style="z-index: 5;"> 
								</div>-->

								<!-- LAYER NR. 2 -->
								<h1 class="tp-caption tp-resizeme" 
                           data-x="left" data-hoffset="650" 
                          data-y="150" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          <span>Kujdes në cilësi</span> <br>Që ju mund të<span class="color"> Besoni</span>
                       </h1>
								<!-- LAYER NR. 2 -->
                        <p class="tp-caption tp-resizeme"
                           data-x="left" data-hoffset="650"  
                          data-y="290" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="800"
                          style="z-index: 9;"> Elitaplus, është qendër e specializuar<br> për sëmundje Endokrinologjike.
                        </p>
                       
							</li>
							
							<li data-index="rs-130" data-transition="slideleft" data-slotamount="default" data-rotate="0"  data-title="Shërbimet Mjeksore" data-description="Elitaplus, është qendër e specializuar
për sëmundje Endokrinologjike.">
								<img src="images/home-page-pic1.jpg"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="15"
                          data-y="150" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          <span>Shërbimet Mjeksore</span> <br> Që ju mund të <span class="color">Besoni</span>
                        </h1>
                        <p class="tp-caption tp-resizeme"
                          data-x="left" data-hoffset="15"
                          data-y="290" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="800"
                          style="z-index: 9;"> Elitaplus, është qendër e specializuar<br> për sëmundje Endokrinologjike.
                        </p>
							</li>
						
							<li data-index="rs-131" data-transition="slideleft"   data-rotate="0" data-title="Teknologji më të mirë" data-description="Elitaplus, është qendër e specializuar
për sëmundje Endokrinologjike.">
								<img src="images/home-page-pic3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="15"
                          data-y="150" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          <span>Teknologji më të mirë</span> <br> Që ju mund të <span class="color">Besoni</span>
                        </h1>
                        <p class="tp-caption tp-resizeme"
                          data-x="left" data-hoffset="15"
                          data-y="290" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="800"
                          style="z-index: 9;"> Elitaplus, është qendër e specializuar<br> për sëmundje Endokrinologjike.
                        </p>
							</li>
                     <li data-index="rs-132" data-transition="slideleft"   data-rotate="0" data-title="Kujdes në cilesi" data-description="Elitaplus, është qendër e specializuar
për sëmundje Endokrinologjike.">
								<img src="images/home-page-pic4.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <h1 class="tp-caption tp-resizeme" 
                          data-x="left" data-hoffset="650"  
                          data-y="150" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="500" 
                          data-splitin="none" 
                          data-splitout="none" 
                          style="z-index: 6;">
                          <span>Kujdes në cilësi</span> <br> Që ju mund të <span class="color">Besoni</span>
                        </h1>
                        <p class="tp-caption tp-resizeme"
                          data-x="left" data-hoffset="650" 
                          data-y="290" 
                          data-transform_idle="o:1;"
                          data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" 
                          data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" 
                          data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                          data-mask_out="x:0;y:0;s:inherit;e:inherit;" 
                          data-start="800"
                          style="z-index: 9;"> Elitaplus, është qendër e specializuar<br> për sëmundje Endokrinologjike.
                        </p>
							</li>
							<!-- SLIDE  -->
						</ul>
							
					</div>
				</div>
 <!-- END REVOLUTION SLIDER -->



<section>
  <div class="container index3_appointment padding">
    <div class="row">
      <div class="col-md-9 appointment_form">
          <div id='add_app' > <?php  if(isset($_SESSION['ins_app_succ'])){echo $_SESSION['ins_app_succ'];$_SESSION['ins_app_succ']=''; }?> </div>
        <h2 class="heading">Cakto Terminet</h2>
        <hr class="heading_space">
        
<!--       FORM -->
       
    <form class="callus" id="app_form"  method="post" action="include/insert_app.php" onsubmit="return app_val()" >
              <div class="row">
                
                 <div class="col-md-12">
                    <div id="result" class="text-center form-group"></div>
                 </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="text" id="app_name" name="app_name"  placeholder="Emri Mbiemri" />
                  </div>
                </div>
                  
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="email" name="app_email" id="app_email"  placeholder="Email"  />
                  </div>
                </div>
                  
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="tel"  name="app_telephone" id="app_telephone" placeholder="Nr.Telefonit"  />
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                   <?php 
                                  
                      $date = date('Y-m-d');
                                                                                                                                                                                                $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                      $check_sun = date("l",strtotime($date));
                      $check_sun = strtolower($check_sun);
                    if($check_sun == 'sunday'){
                      $date = date('Y-m-d', strtotime('+1 day', strtotime($date)));
                        
                    }
                      
                      ?>
                      <!--    FORM     -->
                    <input type="date" class="form-control " placeholder="Data e terminit" id="app_date"  onchange="on_ch(this.value);no_sun();w_date()" name="app_date" value="<?php echo $date; ?>"  />
                  </div>
                </div>
                  <div class="col-md-6">
                  <div class="form-group">
                      
                      <select class="form-control" placeholder="Koha e terminit HH:MM" id="app_time" name="app_time" >
                          
                          
                    </select>

                  </div>
                </div>
                  
                  <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Filan Fisteki" id="doc_name" name="doc_name" value="Dr.Erduan Sefedini" disabled  />
                  </div>
                </div>



                  <div class="form-group">
                     <div class="btn-submit button3">
                    <input type="submit" id="btn_app_submit" value="Cakto" />
                    </div>
                  </div>
                </div>
            </form>
      </div>
      <div class="col-md-3">
      </div>
    </div>
    <div class="col-md-3"></div>
  </div>
</section>

<!--Latest News-->
<section id="news" class="bg_grey padding">
  <div class="container">
    <div class="row">
        
      <div class="col-md-12">
      <h2 class="heading">Lajmet e fundit</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="specialists_wrap_slider">
          <div id="news-slider" class="owl-carousel">
              <?php
                $query = "SELECT * FROM lajmet_fundit WHERE 1";

            $select_news = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_news)){
                $ls_id = $row['id_ls'];
                $title = $row['titulli'];
                $pershkrimi=$row['pershkrimi'];
                $img = $row['foto'];
    
               
            
               
               echo' 
              
              
             
              
            <div class="item">
              <div class="news_content">
              
               <img  src='.$img.' alt="Lajmet">
             
               <div class="comment_text">
               ' ; ?>
                            <?php 
                                    $limit = 200;
                                    
                                    if (strlen($pershkrimi) < $limit) {
                                            echo '<h3><a   data-fancybox-group="gallery" href="lajmet-detail.php?q_id='.$ls_id.'" title="'.$title.' - '.$pershkrimi.'">'.$title.'</a></h3>
                 <p>'.$pershkrimi.'</p>';
                                        } else {
                                            
                                                                                                                                                               echo ' <h3><a href="lajmet-detail.php?q_id='.$ls_id.'"  data-fancybox-group="gallery" title="'.$title.' - '.$pershkrimi.'">'.$title.'</a></h3> 
                 ';
                                            echo substr($pershkrimi,0, $limit);
                                            echo '<br><a data-fancybox-group="gallery" href="lajmet-detail.php?q_id='.$ls_id.'" title="'.$title.' - '.$pershkrimi.'"><b>më shumë..</b></a>';
                                        }
               
               
               
               
              echo'  
         
               </div>
              </div>
            </div>'
              
                    ;} ?>              
                        
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<!-- testinomial -->
<section id="testinomial" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <div id="testinomial-slider" class="owl-carousel text-center">
        <div class="item">
          <h3>Kudo që arti i mjekësisë është i dashur, ka edhe një dashuri për njerëzimin.</h3>
          <p>HIPPOCRATES, <span>ElitaPlus</span></p>
        </div>
        <div class="item">
          <h3>Gjithëmonë qesh kur të mundesh, është mjekimi me i lirë.</h3>
          <p>George Gordon Byron, <span> ElitaPlus</span></p>
        </div>
        <div class="item">
          <h3>Ka shumë njerz në botë të cilët janë duke vdekur për një copë bukë,por ka shumë më tepër që vdesin për pak dashuri.</h3>
          <p>Nëna Terez, <span>ElitaPlus</span></p>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>



<!--Footer-->
<footer class="padding-top bg_blue">
 <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>
    
    
   
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script>
     
     $(function() {
    $("#app_date").datepicker({
        minDate:0,
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        beforeShowDay: function(date) {
            var day = date.getDay();
             return [(day != 0), ''];
    }
    });
});     
           
    </script>
    
    
    
<!--<script src="js/jquery-2.2.3.js" type="text/javascript"></script>-->
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.parallax-1.1.3.js"></script>
<script src="js/parallax.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/on_load_ex_script.js" type="text/javascript"></script>

</body>
</html>
