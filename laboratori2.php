<!doctype html>

   <?php include'include/db.php'; ?>

<html lang="en">

<?php require'include/head.php'; ?>


<body>

<!--Loader-->



<!--Top bar-->



<header id="main-navigation">
  <?php
    require'include/header.php';
    ?>
</header>

<!--Page header & Title-->
<section id="page_header">

<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Laboratori</h2>
         <div class="page_link"><a href="index.php">Ballina</a><span><i class="fa fa-long-arrow-right"></i>Laboratori</span></div>
      </div>
    </div>
  </div>
</div>  


</div>

    

</section>
<section id="service" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Analizat</h2>
      <hr class="half_space">
        <div class="row">
            
          <!--  SEARCH KATEGORIA          -->
        <div class="col-md-3 pull-right half_space"   >
               
              <label> Analizat ne baze Kategoris </label>
                 
            <select class="form-control "  onchange="search_analiza_kat();" id="sch_analiza_k">
                    
                <option value="all" >Te gjitha kategorit e Analizat</option>
                <?php 

          $query = 'select id_kategoria_analizave,emri from kategori_analiza order by emri asc';
          
          $select_k_analiza = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['emri'];
           
                      echo  '<option value="'.$ka_id.'">' .$kategoria. '</option>';
                        
                }    ?>
                    </select>
                </form>
                 
            
            
            </div>
          <!--  SEARCH KEYUP   -->
          <div class="col-md-3 pull-right half_space"   >
               
              <label> Kerko analizen  </label>
                 
            <input class="form-control " placeholder="Kerko Analizen" onkeyup="search_analiza_spec()" id="sch_analiza_a">                   
                                                
                </form>
                 
            
            
            </div>
        </div>
          
      <div class="service_wrapper">
      <ul class="items">
       <?php 
         if(isset($_GET['q_sch_a'])){
          $v_q_sch_a = mysqli_real_escape_string($dbc,$_GET['q_sch_a'] );
             }
          if(!isset($v_q_sch_a) ||  $v_q_sch_a == ' ' ||  $v_q_sch_a == '' ){
              
              if(isset($_GET['q_sch'])){
           $v_q_sch= mysqli_real_escape_string($dbc,$_GET['q_sch']);
              }
          $v_sch = empty($v_q_sch) ? '' : $v_q_sch ;
          
          
          $query = 'select id_kategoria_analizave,emri from kategori_analiza order by emri asc';
          $select_k_analiza = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['emri'];
               if($v_sch == $ka_id or $v_sch == 'all'){
                  
                   $v_sch_k = $v_sch;
                   
               }else{
                   $v_sch_k = false;
                   $v_sh_a = $v_sch;
                   
               }
                }
                          
                      
            if ($v_sch == '' or $v_sch == 'all'){  
                
          $query = 'select id_kategoria_analizave,emri from kategori_analiza order by emri asc';
            }else{
          $query = 'select id_kategoria_analizave,emri from kategori_analiza where id_kategoria_analizave ='.$v_sch.' order by emri asc';
            }
          $select_k_analiza = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_k_analiza)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['emri'];
           
          
          ?>
        <li>
        <?php if($v_sch == '' or $v_sch == 'all' ) { ?>
        <a href="#" ><span style="display:none" id="get_kategoria_sch">all </span><?php echo $kategoria; ?></a>
          <ul class="sub-items bg_grey">
           <?php }else{ ?>
              <a href="#" id="get_kategoria_sch"><?php echo $kategoria; ?></a>
          <ul class="sub-items bg_grey">
              <?php
              
              }
           
              ?>
    <?php         $query= 'SELECT ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza, a.pershkrimi
FROM kategori_analiza ka
INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave where ka.id_kategoria_analizave = '.$ka_id.' order by ka.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['kategoria'];
            $analiza = $row['analiza'];
            $pershkrimi = $row['pershkrimi'];

          ?>
               <li>
                <div class="row"> 
                  <div class="col-md-12 accordion-text" >
                  <h4><?php echo $analiza ?></h4>
                  <p class="half_space"><?php echo $pershkrimi ?></p>
                  
                </div>
              </div>
            </li>
              <?php } ?>
          </ul>
        </li>
      
     <?php } } else { ?>
             <li>
        <?php $v_sch = $_GET['q_sch_a'];?>
                 
                     <?php         $query= 'SELECT ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza, a.pershkrimi
FROM kategori_analiza ka
INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave where a.emri like  "%'.$v_sch.'%" order by ka.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['kategoria'];
            $analiza = $row['analiza'];
            $pershkrimi = $row['pershkrimi'];
            $rowcount=mysqli_num_rows($select_analizat);
           }
          ?>
            <?php if(isset($rowcount)){  ?>     
                 
        <a href="#" ><span style="display:none"
                           id="get_kategoria_sch">none</span>Rezultati i kerkimit per: <span id="search_txt_a"></span> 
            <?php echo $v_sch; ?> 
            <span class="pull-right" style="margin-right:50px;margin-top:7px;color:#ff3332;font-size:14px">Jane gjetur <?php echo $rowcount; ?> perputhje</span> <?php }else{ echo 'Nuk eshte gjet asnje analize me :'.$v_sch;} ?>
                 </a>
          <ul class="sub-items bg_grey">
                    <?php         $query= 'SELECT ka.id_kategoria_analizave, ka.emri kategoria, a.emri analiza, a.pershkrimi
FROM kategori_analiza ka
INNER JOIN analizat a ON a.id_kategoria_analizave = ka.id_kategoria_analizave where a.emri like  "%'.$v_sch.'%" order by ka.emri asc'; 
            
                $select_analizat = mysqli_query($dbc, $query);

           while($row = mysqli_fetch_assoc( $select_analizat)){
            $ka_id = $row['id_kategoria_analizave'];
            $kategoria = $row['kategoria'];
            $analiza = $row['analiza'];
            $pershkrimi = $row['pershkrimi'];
            $rowcount=mysqli_num_rows($select_analizat);
           
          ?>

               <li>
                <div class="row"> 
                  <div class="col-md-12 accordion-text" >
                  <h4><?php echo $analiza ?></h4>
                  <p class="half_space"><?php echo $pershkrimi ?></p>
                  
                </div>
              </div>
            </li>
              <?php } }  ?>
          </ul>
        </li>
        
      </ul>
      
    </div>
      </div>
    </div>
  </div>
</section>




<!--Footer-->
<footer class="padding-top dark">
  <?php
    require'include/footer.php';
    ?>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/jquery.geolocation.edit.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/slider.js" type="text/javascript"></script>
<script src="js/owl.carousel.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/functions.js" type="text/javascript"></script>
<script src="js/search_analiza.js" type="text/javascript"></script>

</body>
</html>
