// shfaq te gjitha terminet 
function show_terminet(){
         var v_date = document.getElementById('data_terminit_a').value;
        
         var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('show_terminet').innerHTML =xmlhttp.responseText;
               
                }
                   
              }
                  xmlhttp.open("GET","include/show_terminet.php?v_date="+v_date,true);
        xmlhttp.send();

        
};

// shfaq termin sipas ids
function show_terminet_by_id(t_id){
         var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('show_termini_id').innerHTML =xmlhttp.responseText;

                }
                   
              }

                  xmlhttp.open("GET","include/show_terminet2.php?t_id="+t_id,true);
        xmlhttp.send();
    document.getElementById('a_btn_shto_l').style.display = 'none';
    document.getElementById('a_btn_c_ch_dt').style.display = 'none';
    document.getElementById('a_show_terminet1').style.display = 'block';
    document.getElementById('a_show_terminet2').style.display = 'block';
    document.getElementById('shto_termini_ad').style.display = 'none';
    document.getElementById('show_termini_id').style.display = 'block';
    document.getElementById('a_btn_ndrysho').style.display = 'inline';
    document.getElementById('a_btn_anulo').style.display = 'inline';
    document.getElementById('a_btn_ch_dt').style.display = 'inline';
    
  $('html, body').animate({
        scrollTop: $("#a_ter_form").offset().top
    },1000);
    $('#a_ter_em').focus();
        
};

// perditso terminin permes butonin
function update_termini(){
    var t_id = document.getElementById('termini_id').innerHTML;
    if(confirm('Deshironi te perditsoni te dhenat')){
        document.getElementById('a_ter_form').action = 'include/update_terminet.php?t_id='+t_id;
        show_terminet_by_id(t_id);
        return true;
    }else{
        return false;
    }

    
};

//largo terminin permes butonit
function delete_terminet_btn(){
    var t_id = document.getElementById('termini_id').innerHTML;
    var t_emri = document.getElementById('a_ter_em').value;
    var t_data = document.getElementById('a_ter_data').value;
    if(confirm('Deshironi ta largoni terminin me numer: '+t_id + ' - ' +t_emri + ' - ' +t_data )){
    var xmlhttp = new XMLHttpRequest();
                  xmlhttp.open("GET","include/show_terminet2.php?t_d_id="+t_id,true);
        xmlhttp.send();  
    show_terminet();
        f_largo_btn()
        return true;
    } else{
    return false;
    }
}

// shfaq terminet sipas emrit ne search
function show_terminet_by_name(v_name){
        if( v_name == "" || v_name == ' '){
            show_terminet();
        }else{
            var col_span_v = ($("th").attr('colspan',10));
            
     var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('show_terminet').innerHTML =xmlhttp.responseText;
               
                }
                   
              }
                  xmlhttp.open("GET","include/show_terminet2.php?v_name="+v_name,true);
        xmlhttp.send();
}
}

//konfirmo terminin permes id
function konfirmo_terminet_by_id(obj,t_id){
    
    	var t_emri = $(obj).attr('class');
    if(confirm('Deshironi te konfirmoni terminin: '+t_id+'-'+t_emri)){
    var xmlhttp = new XMLHttpRequest();
                  xmlhttp.open("POST","include/show_terminet2.php?t_k_id="+t_id,true);
        xmlhttp.send();
     show_terminet();
    var check_update = document.getElementById('termini_id');
    if(check_update != null){
        show_terminet();
        show_terminet_by_id(t_id);
    }
        return true;
    }else{
    return false;
}
    };

//largo terminin permes ids
function delete_terminet_by_id(obj,t_id){
    
    var t_emri = $(obj).attr('class');
    if(confirm('Deshironi ta largoni terminin me numer: '+t_id+'-'+t_emri)){
    var xmlhttp = new XMLHttpRequest();
                  xmlhttp.open("GET","include/show_terminet2.php?t_d_id="+t_id,true);
        xmlhttp.send();  
    show_terminet();
    var check_update = document.getElementById('termini_id');
    if(check_update != null){
        show_terminet_by_id(t_id);
    }
        return true;
    }else{
    return false;
        }
};

// nese ndrohet data shfaq te dhenat tjera
function on_ch(t_date){
        document.getElementById('a_ter_data_n').value = t_date;
            var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('a_ter_time_n').innerHTML =xmlhttp.responseText;
               
                }
                   
              }
                
                   xmlhttp.open("GET","include/time.php?v_date="+t_date,true);
        xmlhttp.send();

                
            };

//ONLOAD AJAX
function f_load(){
            var search_txt = document.getElementById('sch_termini_a').value;
            if(search_txt != null){
                show_terminet_by_name(search_txt);
            }
            var t_date = document.getElementById('a_ter_data_n').value;
         
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('a_ter_time_n').innerHTML =xmlhttp.responseText;
               
                }
                   
              }
                   xmlhttp.open("GET","include/time.php?v_date="+t_date,true);
        xmlhttp.send();
    
};

//NO SUNDAY
function no_sun(){
    var v_date = document.getElementById('a_ter_data_n').value;

    var v_da = new Date(v_date);
    
    
    var get_day = v_da.getDay();
    
    var day_plus = 1;
    var next_day = v_da.setDate(v_da.getDate() + day_plus);
    v_da = new Date(next_day);
    var get_day_of_next = v_da.getDay();

    var v_y  = v_da.getFullYear();
    var v_m  = v_da.getMonth();
    var v_d  = v_da.getDate();
    var v_next_day; 
    if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
    if (get_day === 0 ){
        document.getElementById('a_ter_data_n').value =v_next_day ;
        alert('Dita e diel eshte pushim ju lutem caktoni terminet nga e Hena ne te Shtune');
        document.getElementById('a_ter_data_n').focus();
       f_load();
   }
   
};

//NO SUNDAY
function ch_no_sun(){
    var v_date = document.getElementById('a_ter_data').value;

    var v_da = new Date(v_date);
    
    
    var get_day = v_da.getDay();
    
    var day_plus = 1;
    var next_day = v_da.setDate(v_da.getDate() + day_plus);
    v_da = new Date(next_day);
    var get_day_of_next = v_da.getDay();

    var v_y  = v_da.getFullYear();
    var v_m  = v_da.getMonth();
    var v_d  = v_da.getDate();
    var v_next_day; 
    if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
    if (get_day === 0 ){
        document.getElementById('a_ter_data').value =v_next_day ;
        alert('Dita e diel eshte pushim ju lutem caktoni terminet nga e Hena ne te Shtune');
        document.getElementById('a_ter_data').focus();
       f_load();
   }
   
};

// no - date new_t
function new_t_date(){
    var v_date = document.getElementById('a_ter_data_n').value;
    var v_da = new Date(v_date); 
    var v_today = new Date;
    v_da.setHours(0,0,0,0);
    v_today.setHours(0,0,0,0);
    
    if(v_da < v_today){
        alert('Data eshte gabim ')
    var v_y  = v_today.getFullYear();
    var v_m  = v_today.getMonth();
    var v_d  = v_today.getDate();
         var v_next_day;
        if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
        document.getElementById('a_ter_data_n').value = v_next_day;
        }
        document.getElementById('a_ter_data_n').focus;
        
        
        f_load();
    }

// no - date old_t
function ch_t_date(){
    var v_date = document.getElementById('a_ter_data').value;
    var v_da = new Date(v_date); 
    var v_today = new Date;
    v_da.setHours(0,0,0,0);
    v_today.setHours(0,0,0,0);
    
    if(v_da < v_today){
        alert('Data eshte gabim ')
    var v_y  = v_today.getFullYear();
    var v_m  = v_today.getMonth();
    var v_d  = v_today.getDate();
         var v_next_day;
        if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
        document.getElementById('a_ter_data').value = v_next_day;
        }
        document.getElementById('a_ter_data').focus;
        
        
        f_load();
    }

//Shfaq mesazhin per query
function show_ins_app_succ(){
    
     
    setTimeout(function(){
         add_app.style.display = "none";       
    },7000);
    
   };

/* Shto Butoni */
function f_shto_btn(){
    document.getElementById('a_btn_shto_a').style.display = 'none';
    document.getElementById('a_btn_largo_a').style.display = 'inline-block';
    document.getElementById('a_btn_shto_l').style.display = 'inline';
    document.getElementById('a_show_terminet1').style.display = 'block';
    document.getElementById('a_show_terminet2').style.display = 'block';
    document.getElementById('shto_termini_ad').style.display = 'block';
    document.getElementById('show_termini_id').style.display = 'none';
    document.getElementById('a_btn_ndrysho').style.display = 'none';
    document.getElementById('a_btn_anulo').style.display = 'none';  
    document.getElementById('a_btn_ch_dt').style.display = 'none';
    document.getElementById('a_btn_c_ch_dt').style.display = 'none';
        var v_today = new Date;
        v_today.setHours(0,0,0,0);
        var day_plus = 1;
        var next_day = v_today.setDate(v_today.getDate() + day_plus);
        var get_day_of_next = v_today.getDay();
        if(get_day_of_next === 0){
             var next_day = v_today.setDate(v_today.getDate() + day_plus);
        }
    var v_y  = v_today.getFullYear();
    var v_m  = v_today.getMonth();
    var v_d  = v_today.getDate();
         var v_next_day;
        if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
        f_load();
   $('html, body').animate({
        scrollTop: $("#a_ter_form").offset().top
    },1000);
    $('#a_ter_em').focus();
            
    };

// largo shto/update
function f_largo_btn(){
    document.getElementById('a_btn_shto_a').style.display = 'inline-block';
    document.getElementById('a_btn_largo_a').style.display = 'none';
    document.getElementById('a_btn_shto_l').style.display = 'inline';
    document.getElementById('a_show_terminet1').style.display = 'none';
    document.getElementById('a_show_terminet2').style.display = 'none';
    document.getElementById('shto_termini_ad').style.display = 'none';
    document.getElementById('show_termini_id').style.display = 'none';
    document.getElementById('a_btn_ndrysho').style.display = 'none';
    document.getElementById('a_btn_anulo').style.display = 'none';  
    document.getElementById('a_btn_ch_dt').style.display = 'none';
    document.getElementById('a_btn_c_ch_dt').style.display = 'none';
    return false;
}

//Shto terminin duke paraqitur kohes
function f_shto_termin_id(t_koha){
    var search_txt = document.getElementById('sch_termini_a').value;
            if(search_txt != null){
                show_terminet_by_name(search_txt);
            }
    document.getElementById('a_btn_shto_l').style.display = 'inline';
    document.getElementById('a_show_terminet1').style.display = 'block';
    document.getElementById('a_show_terminet2').style.display = 'block';
    document.getElementById('shto_termini_ad').style.display = 'block';
    document.getElementById('show_termini_id').style.display = 'none';
    document.getElementById('a_btn_ndrysho').style.display = 'none';
    document.getElementById('a_btn_anulo').style.display = 'none';
    document.getElementById('a_btn_ch_dt').style.display = 'none';
    document.getElementById('a_btn_c_ch_dt').style.display = 'none';
     var s_data = document.getElementById('data_terminit_a').value ;   
     var n_data = document.getElementById('a_ter_data_n').value ;   
        document.getElementById('a_ter_time_n').value = t_koha;
        var v_today = new Date;
        v_today.setHours(0,0,0,0);
        var day_plus = 1;
        var next_day = v_today.setDate(v_today.getDate() + day_plus);
        var get_day_of_next = v_today.getDay();
        if(get_day_of_next === 0){
             var next_day = v_today.setDate(v_today.getDate() + day_plus);
        }
    var v_y  = v_today.getFullYear();
    var v_m  = v_today.getMonth();
    var v_d  = v_today.getDate();
         var v_next_day;
        if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
   $('html, body').animate({
        scrollTop: $("#a_ter_form").offset().top
    },1000);
    $('#a_ter_em').focus();
            
    };

// Plus Minus Data
function pm_data_a(v_pm){
        var v_date = document.getElementById('data_terminit_a').value;
        var v_da = new Date(v_date);
        v_pm = Number(v_pm);
        var day_plus = v_pm;
        v_da.setDate(v_da.getDate() + day_plus);

       var v_y  = v_da.getFullYear();
       var v_m  = v_da.getMonth();
       var v_d  = v_da.getDate();
            var v_next_day;
           if (v_d <= 9 ){
           v_d = '0'+v_d;
       }
         
            if(v_m  < 9){
                v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
                }else{
                    v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
                }
    document.getElementById('data_terminit_a').value = v_next_day;
    document.getElementById('a_ter_data_n').value = v_next_day;
    on_ch(v_next_day);
    show_terminet();
            
}

// shfaq ch daten/kohen
function ch_data_time(){
    document.getElementById('ch_data_time').style.display = 'block';
    document.getElementById('a_btn_ch_dt').style.display = 'none';
    document.getElementById('a_btn_c_ch_dt').style.display = 'inline';
    document.getElementById('a_ter_data').disabled = "true";
    document.getElementById('a_ter_time').disabled = "true";
    document.getElementById('ch_ter_data').disabled = false;
    document.getElementById('ch_ter_time').disabled = false;    
    var t_date = document.getElementById('ch_ter_data').value;
         
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
               document.getElementById('ch_ter_time').innerHTML =xmlhttp.responseText;
               
                }
                   
              }
                   xmlhttp.open("GET","include/time.php?v_date="+t_date,true);
        xmlhttp.send();
    
            
       
    return false;
}

// no - ch date
function ch_t_date(){
    var v_date = document.getElementById('ch_ter_data').value;
    var v_da = new Date(v_date); 
    var v_today = new Date;
    v_da.setHours(0,0,0,0);
    v_today.setHours(0,0,0,0);
    
    if(v_da < v_today){
        alert('Data eshte gabim ')
    var v_y  = v_today.getFullYear();
    var v_m  = v_today.getMonth();
    var v_d  = v_today.getDate();
         var v_next_day;
        if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
        document.getElementById('ch_ter_data').value = v_next_day;
        }
        document.getElementById('ch_ter_data').focus;
        
        
        ch_data_time();
    }

// no sun ch date
function ch_dt_no_sun(){
    var v_date = document.getElementById('ch_ter_data').value;

    var v_da = new Date(v_date);
    
    
    var get_day = v_da.getDay();
    
    var day_plus = 1;
    var next_day = v_da.setDate(v_da.getDate() + day_plus);
    v_da = new Date(next_day);
    var get_day_of_next = v_da.getDay();

    var v_y  = v_da.getFullYear();
    var v_m  = v_da.getMonth();
    var v_d  = v_da.getDate();
    var v_next_day; 
    if (v_d <= 9 ){
        v_d = '0'+v_d;
    }

    if(v_m  < 9){
            v_next_day =v_y+'-0'+(v_m+1)+'-'+v_d;
        }else{
            v_next_day =v_y+'-'+(v_m+1)+'-'+v_d;
        }
    if (get_day === 0 ){
        document.getElementById('ch_ter_data').value =v_next_day ;
        alert('Dita e diel eshte pushim ju lutem caktoni terminet nga e Hena ne te Shtune');
        document.getElementById('ch_ter_data').focus();
       f_load();
   }
   
};

// cancel ch 
function ch_dt_cancel(){
    document.getElementById('a_btn_c_ch_dt').style.display = 'none';
    document.getElementById('a_btn_ch_dt').style.display = 'inline';
    document.getElementById('ch_data_time').style.display = 'none';
    document.getElementById('ch_ter_data').disabled = "true";
    document.getElementById('ch_ter_time').disabled = "true";
    document.getElementById('a_ter_data').disabled = false;
    document.getElementById('a_ter_time').disabled = false;
        
    
    return false;
}