<!-- CONTENT-WRAPPER SECTION END-->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2016 ElitaPlus | By : <a href="https://www.facebook.com/Crow-Development-611776368981759/" target="_blank">Crow</a>
                </div>

            </div>
        </div>
    </footer>
<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>
    <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY SCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
   
    <script src="assets/js/function.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
</body>
</html>