<?php require'include/header.php'; 
 if($_SESSION['roli'] == '1'){
?>
      <div class="content-wrapper">
        <div class="container">
            <div class="row">
                         <div class="panel-body" id="butonishto"> 
                    <a href="doktoret.php" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> &nbsp; Shto Staf </a>
                    
                     </div>
                        <h1 class="page-head-line">Stafi</h1>
                         <div class="panel panel-default">
                          
                        <div class="panel-heading">
                            Lista e stafit
                        </div>
                        <div class="panel-body">
                                                                    <?php
  if (isset($_GET["msg"]) && $_GET["msg"] == 'sukses') {
echo "<p class='bg-success' > Stafi u ndryshua me sukses! </p>";
      header("refresh:1; url=doktoret-detail.php ");
}    elseif(isset($_GET["msg"]) && $_GET["msg"] == 'delete') {
echo "<p class='bg-success' > Stafi u fshi me sukses! </p>";
    
      header("refresh:1; url=doktoret-detail.php ");
} elseif(isset($_GET["msg"]) && $_GET["msg"] == 'failed') {
echo "<p class='bg-failed' > Nuk perfundoj me sukses - ka ndodhu nje gabim! </p>";
    
      header("refresh:1; url=doktoret-detail.php ");
}
    ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Emri</th>
                                            <th>Mbiemri</th>
                                            <th>Pozita</th>
                                            <th>Profesioni</th>
                                            <th>Pershkrimi</th>
                                            <th>Foto</th>
                                            <th>-</th>
                                            <th>-</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                <?php                                         
                    $query = "SELECT * FROM staf";

                    $select_users = mysqli_query($dbc, $query);

                    while($row = mysqli_fetch_assoc($select_users)){

                    $staf_id = $row['id_staf'];
                    $emri = $row['emri'];
                    $mbiemri = $row['mbiemri'];
                    $pozita = $row['pozita'];
                    $profesioni=$row['profesioni'];    
                    $pershkrimi = $row['pershkrimi'];
                    $foto= $row['foto'];     
                        
                        echo '<tr>';
                        echo '<td>'.$staf_id.'</td>';
                        echo '<td>'.$emri.'</td>';
                        echo '<td>'.$mbiemri.'</td>';
                        
                        $query = "SELECT * FROM staf_pozita WHERE id_staf_pozita = $pozita ";
    $select_user_role = mysqli_query($dbc, $query);
    while($rows = mysqli_fetch_assoc($select_user_role)){

        $sfat_role = $rows['emri'];

    }
                        
                        echo '<td>'.$sfat_role.'</td>';
                        echo '<td>'.$profesioni.'</td>';
                        echo '<td>'.$pershkrimi.'</td>';
                        echo '<td>'.$foto.'</td>';
                                        
                                        
                    echo "<td><a href='doktoret.php?edit_stafi={$staf_id}'><button class='btn btn-primary'><i class='fa fa-edit '></i>Ndrysho</button></a></td>";
                  //    echo '<td><button name="delete" method="post" class="btn btn-danger"><i class="fa fa-pencil"></i><a href='doktoret.php?delete={$staf_id}'>Delete</a></button></td>';
                        echo "<td><a href='doktoret.php?delete={$staf_id}'><button class='btn btn-danger' onclick='return MyFunction();'><i class='fa fa-trash-o '></i>  Fshij</button></a></td>";
                        echo'</tr>';

    
        
    }; ?>                    
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
            </div>
          </div>
</div>
                    <?php require'include/footer.php';  
        }else{ echo "<h1>'Nuk keni autorizim per te vazhduar'</h1>";
        header("refresh:3; url=../terminet.php");} ?>
