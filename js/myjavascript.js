function contact(){
    
    var f_name = document.getElementById('f_name').value;
    var l_name = document.getElementById('l_name').value;
    var phone = document.getElementById('phone').value;
    var email = document.getElementById('email').value;
    var message= document.getElementById('message').value;

    
    
    
    
     //Emri
    if(f_name == null || f_name == '' || f_name ==' ') {
        alert("Emri nuk duhet te jete i zbrazet");
        document.getElementById('f_name').focus();
        return false;
        
    }
 
     if(f_name.length <3 || f_name.length > 30) {
        alert("Emri duhet te jete prej 3-30 karaktere");
         document.getElementById('f_name').focus();
        return false;
    }
    
    if(!isNaN(f_name)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('f_name').focus();
        return false;
    }
    
    //Mbiemri 
    
    if(l_name == null || l_name == '' || l_name ==' ') {
        alert("Mbiemri nuk duhet te jete i zbrazet");
        document.getElementById('l_name').focus();
        return false;
    }
 
     if(l_name.length <3 || l_name.length > 30) {
        alert("Mbiemri duhet te jete prej 3-30 karaktere");
        document.getElementById('l_name').focus();
        return false;
    }
    
    if(!isNaN(l_name)) {
        alert("Mbiemri nuk duhet te permbaj numra");
        document.getElementById('l_name').focus();
        return false;
    }
    
    
    
    //Telefoni
    if(phone == null || phone == '' || phone ==' ') {
        alert("Telefoni nuk duhet te jete i zbrazet");
        document.getElementById('phone').focus();
        return false;
    }
 
     if(phone.length <9 || phone.length > 15) {
        alert("Telefoni duhet te jete prej 9-15 karaktere");
        document.getElementById('phone').focus();
        return false;
    }
    
    if(isNaN(phone)) {
        alert("Numri i telefonit nuk duhet te permbaj shkronja");
        document.getElementById('phone').focus();
        return false;
    }
    
    
    // Email
    

    if(email == null ||email == '' || email ==' ') {
        alert("Email nuk duhet te jete i zbrazet");
        document.getElementById('email').focus();
        return false;
    }
    //Message
    
    if(message == null ||message == '' || message ==' ') {
        alert("Mesazhi nuk duhet te jete i zbrazet");
        document.getElementById('message').focus();
        return false;
    }
 
    
}