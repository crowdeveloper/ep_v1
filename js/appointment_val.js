 
function app_val(){
    
    var a_name = document.getElementById('app_name').value;
    var a_email = document.getElementById('app_email').value;
    var a_tel = document.getElementById('app_telephone').value;
    var a_date = document.getElementById('app_date').value;
    var a_time = document.getElementById('app_time').value;

    
    
     //Emri
    if(a_name == null || a_name == '' || a_name ==' ') {
        alert("Emri dhe Mbiemri nuk duhet te jene i zbrazet");
        document.getElementById('app_name').focus();
        return false;
        
    }
 
     if(a_name.length <3 ) {
        alert("Emri dhe Mbiemri  duhet te kete me shume se 3 karaktere");
         document.getElementById('app_name').focus();
        return false;
    }
    
    if(!isNaN(a_name)) {
        alert("Emri nuk duhet te permbaj numra");
        document.getElementById('app_name').focus();
        return false;
    }
    
    //Telefoni
    if(a_tel == null || a_tel == '' || a_tel ==' ') {
        alert("Telefoni nuk duhet te jete i zbrazet");
        document.getElementById('app_telephone').focus();
        return false;
    }
 
    
    if(isNaN(a_tel)) {
        alert("Numri i telefonit nuk duhet te permbaj shkronja");
        document.getElementById('app_telephone').focus();
        return false;
    }
        
     if(a_tel.length <9 || a_tel.length > 15) {
        alert("Telefoni duhet te jete prej 9-15 karaktere");
        document.getElementById('app_telephone').focus();
        return false;
    }
    
    
    // Data
    

    if(a_date == null ||a_date == '' || a_date ==' ') {
        alert("Ju lutem caktoni Daten");
        document.getElementById('app_date').focus();
        return false;
    }
 
    //Koha e terminit
    
    if(a_time == null ||a_time == '' || a_time ==' ' || a_time == "Koha e terminit HH:MM" ) {
        alert("Ju lutem caktoni Kohen e Terminit");
        document.getElementById('app_time').focus();
        return false;
    };
    
   
    
}